#################################### IMPORT DATA ######################################
import pickle
from sklearn.feature_extraction.text import CountVectorizer
import pandas as pd
import numpy as np
from nltk.corpus import stopwords 

training_data = pd.read_csv("apps/classification/movies_metadata.csv") 


################################ DATA PREPROCESSING ##################################
from nltk.corpus import stopwords						
from nltk.tokenize import word_tokenize

def clean_stopwords(syn): 								# Clean stopwords function
    stop_words = set(stopwords.words('english'))
    clean = []
    for sent in syn:
        new = []
        token = word_tokenize(sent)
        for word in token:
            if word not in stop_words:
                new.append(word)
        clean.append(' '.join(new))
    
    return(clean)


import re

synopsis = training_data.Plot                             # Get movies synopsis
genre = training_data.Genre1                              # Get movies genre
syn = []												  # Create empty array to store processed synopsis

for sen in range(0,len(synopsis)):
    document = re.sub(r'\W', ' ', str(synopsis[sen]))		# special character and punctuation
    document = document.lower()								# lower case
    document = re.sub(r'\s+[a-zA-Z]\s+', ' ', document)		# single character
    document = re.sub(r'\s+', ' ', document, flags=re.I)	# double space
    syn.append(document)									# save in "syn" array

clean = clean_stopwords(syn) 								# clean stopwords


############################### CONVERT TEXT INTO NUMBERS ######################################
count_vect = CountVectorizer(stop_words=stopwords.words('english'))
X_train_counts = count_vect.fit_transform(clean)

pickle.dump(count_vect.vocabulary_, open("count_vector.pkl","wb"))		# save vector into model


######################################### TF-IDF ##############################################
from sklearn.feature_extraction.text import TfidfTransformer

tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)

pickle.dump(tfidf_transformer, open("tfidf.pkl","wb"))					# save tf-idf into model


############################ SPLIT DATA INTO TRAIN AND TEST ####################################
from sklearn.neighbors import KNeighborsClassifier  
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X_train_tfidf, genre, test_size=0.25, random_state=42)

########################## TRAINING CLASSIFICATION MODEL - KNN ###################################
classifier = KNeighborsClassifier(n_neighbors=17, n_jobs=1, algorithm='brute')  
classifier.fit(X_train, y_train) 

#SAVE MODEL
pickle.dump(classifier, open("knn.pkl", "wb"))					# save model


############################## PREDICTION FUNCTION #################################################
def main(input_text):
	import pickle
	from sklearn.feature_extraction.text import CountVectorizer
	from sklearn.feature_extraction.text import TfidfTransformer
	
	docs_new = input_text
	docs_new = [docs_new]

	#LOAD MODEL
	loaded_vec = CountVectorizer(vocabulary=pickle.load(open("count_vector.pkl", "rb")))
	loaded_tfidf = pickle.load(open("tfidf.pkl","rb"))
	loaded_model = pickle.load(open("knn.pkl","rb"))

	X_new_counts = loaded_vec.transform(docs_new)
	X_new_tfidf = loaded_tfidf.transform(X_new_counts)
	predicted = loaded_model.predict(X_new_tfidf)

	return predicted[0]