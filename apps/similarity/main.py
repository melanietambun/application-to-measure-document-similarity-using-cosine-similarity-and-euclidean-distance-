def main(input_text1, input_text2):    
    import numpy as np
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    import seaborn as sns
    import math

    import warnings 
    warnings.filterwarnings("ignore")

    # preprocessing
    import os

    import re
    import pandas as pd
    import collections

    def ptb_preprocess(textfile,sering_muncul):
        words=collections.Counter()
        with open(textfile) as textfile:
            for line in textfile:
                words.update(line.lower().split())
        return dict(words.most_common(sering_muncul))

    #menghasilkan fileoutput yaitu ptb.train.output.txt
    file=open("apps/classification/Data/Data1Output.txt","w")
    texttop10000=ptb_preprocess('apps/classification/Data/Data1.txt',10000)
    file.write(str(texttop10000))
    file.close()

    import re
    import pandas as pd

    # file_from = 'D:/'
    # file_dest = 'D:/movie_classification/apps/classification/data/'

    file_dir = 'apps/classification/Data/'
    filename1 = file_dir+input_text1 


    # os.rename(file_from+input_text1,file_dest+input_text1)
    file = open (filename1, 'r')

    corpus = []
    for f in file.readlines():
        corpus.append(f)

    sentence = corpus [:100]

    pd.set_option ('display.max_colwidth', -1)

    new_sentence = [word.lower() for word in sentence]

    import string 
    from string import digits
    string.punctuation 
    sentences = []


    for words in new_sentence:
        for punctuation in string.punctuation:
            words = words.replace(punctuation, "")
            words = words.translate(digits)
        print(words)
        hasil1 = sentences.append(words)
        
        

    import nltk 
    from nltk.corpus import stopwords
    from nltk.tokenize import word_tokenize

    stop_words = set(stopwords.words("english"))
    kalimat = []
    for words in sentences:
        word_tokens = word_tokenize(words)
        for word in (w for w in word_tokens if not w in stop_words):
            kalimat.append(word)

    from nltk.stem import PorterStemmer
    ps = PorterStemmer ()

    for i in range (0,  len(kalimat)):
        if(kalimat[i] != ps.stem(kalimat[i])):
            kalimat[i] = ps.stem(kalimat[i])


    ### Preprocessing Data 2

    #menghasilkan fileoutput yaitu ptb.train.output.txt
    file2=open("apps/classification/Data/Data2Output.txt","w")
    texttop10000=ptb_preprocess('apps/classification/Data/Data2.txt',10000)
    file2.write(str(texttop10000))
    file2.close()

    import re
    import pandas as pd
    filename2 = file_dir+input_text2
    # os.rename(file_from+input_text2,file_dest+input_text2)
    file2 = open (filename2, 'r')


    corpus = []
    for f2 in file2.readlines():
        corpus.append(f2)


    sentence2 = corpus [:100]

    pd.set_option ('display.max_colwidth', -1)

    new_sentence2 = [word2.lower() for word2 in sentence2]

    import string 
    from string import digits
    string.punctuation 
    sentences2 = []

    for words2 in new_sentence2:
        for punctuation in string.punctuation:
            words2 = words2.replace(punctuation, "")
            words2 = words2.translate(digits)
        sentences2.append(words2)

    import nltk 
    from nltk.corpus import stopwords
    from nltk.tokenize import word_tokenize

    stop_words = set(stopwords.words("english"))
    kalimat2 = []
    for words2 in sentences2:
        word_tokens = word_tokenize(words2)
        for word2 in (w for w in word_tokens if not w in stop_words):
            kalimat2.append(word2)

    from nltk.stem import PorterStemmer
    ps = PorterStemmer ()

    for i in range (0,  len(kalimat2)):
        if(kalimat2[i] != ps.stem(kalimat2[i])):
            kalimat2[i] = ps.stem(kalimat2[i])


    ### Memanggil data hasil preprocessing

    #menghasilkan fileoutput yaitu ptb.train.output.txt
    file=open("apps/classification/Data/HsilPreProcessing1.txt","w")
    #texttop10000=ptb_preprocess('Data1.txt',10000)
    file.write(str(kalimat))
    file.close()

    #menghasilkan fileoutput yaitu ptb.train.output.txt
    file2=open("apps/classification/Data/HsilPreProcessing2.txt","w")
    #texttop10000=ptb_preprocess('Data1.txt',10000)
    file2.write(str(kalimat2))
    file2.close()

    # Measuring Similarity

    import numpy as np

    def readFile(fileName):
        return open(fileName, 'r').read()

    text1 = readFile("apps/classification/Data/HsilPreProcessing1.txt")
    text2 = readFile("apps/classification/Data/HsilPreProcessing2.txt")


    from sklearn.feature_extraction.text import CountVectorizer

    cv = CountVectorizer()
    X = np.array(cv.fit_transform([text1, text2]).todense())

    ### Euclidean Distance

    def euclidean_distance(x, y):   
        return np.sqrt(np.sum((x - y) ** 2))

    # print("Dok1 - Dok2 \t", euclidean_distance(X[0], X[1]))

    ### Cosine Similarity

    def cosine_similarity(x, y):
        return np.dot(x, y) / (np.sqrt(np.dot(x, x)) * np.sqrt(np.dot(y, y)))


    # return(cosine_similarity(X[0], X[1]))
    return(cosine_similarity(X[0], X[1]))