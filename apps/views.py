from django.shortcuts import render
from apps.classification import main

def form_index(request):
    return render(request, 'apps/index.html')

def classification(request):
    if request.method == 'POST':
        # text = request.POST['input_text1']
        input_text1 = request.POST['input_text1']
        input_text2 = request.POST['input_text2']
        cal = main.main(input_text1, input_text2)
        content = {'cal': cal}
        return render(request, 'apps/index.html', content)
    return render(request, 'apps/index.html')

def index(request):
    return render(request, 'apps/welcome.html')